#include "TrigFTKByteStream/TrigFTKByteStreamCnv.h"
#include "TrigFTKByteStream/TrigFTKByteStreamTool.h"
#include "../FTKByteStreamDecoderEncoder.h"
#include "../FTKByteStreamDecoderEncoderAux.h"
#include "../FTKDump.h"

DECLARE_CONVERTER( FTK::TrigFTKByteStreamCnv )
DECLARE_COMPONENT( FTK::TrigFTKByteStreamTool )
DECLARE_COMPONENT( FTK::FTKByteStreamDecoderEncoderTool )
DECLARE_COMPONENT( FTK::FTKByteStreamDecoderEncoderAuxTool )
DECLARE_COMPONENT( FTKDump )

