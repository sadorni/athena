#ifndef TRIGT1CAL_CMMCPHitsCOLLECTION_H
#define TRIGT1CAL_CMMCPHitsCOLLECTION_H

#include "AthContainers/DataVector.h"
#include "TrigT1CaloEvent/CMMCPHits.h"

/** Container class for CMMCPHits objects */

using namespace LVL1;
typedef DataVector<CMMCPHits> CMMCPHitsCollection;

#endif
